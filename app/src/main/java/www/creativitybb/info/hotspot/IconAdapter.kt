package www.creativitybb.info.hotspot

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.file_icon.view.*
import kotlinx.android.synthetic.main.file_icon.view.file_name
import kotlinx.android.synthetic.main.file_info.view.*
import java.io.FilenameFilter

class IconAdapter(val context: Context, val item:ArrayList<FileObject>) : RecyclerView.Adapter<IconAdapter.ChildHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildHolder {
        val inflator = LayoutInflater.from(context).inflate(R.layout.file_icon,parent,false)
        return ChildHolder(inflator)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(holder: ChildHolder, position: Int) {
        val current = item[position]
        holder.item.file_name.setText(current.name)
        val dot = current.name.lastIndexOf(".")
        val fileExtension = current.name.substring(dot+1)
           when(fileExtension){
              "apk"->holder.item.file_icon.setImageResource(R.drawable.ic_android)
               "bmp" -> holder.item.file_icon.setImageResource(R.drawable.ic_camera_1908)
               "gif" -> holder.item.file_icon.setImageResource(R.drawable.ic_camera_1908)
               "pdf" -> holder.item.file_icon.setImageResource(R.drawable.ic_pdf)
               "mp4" -> holder.item.file_icon.setImageResource(R.drawable.ic_mp4_1522)
               "jpg" ->  holder.item.file_icon.setImageResource(R.drawable.ic_camera_1908)
               "jpeg" ->  holder.item.file_icon.setImageResource(R.drawable.ic_camera_1908)
                else -> holder.item.file_icon.setImageResource(R.drawable.ic_file_1500)
            }
    }

    class ChildHolder(view: View) : RecyclerView.ViewHolder(view){
        val item = view.icon_container
    }
}