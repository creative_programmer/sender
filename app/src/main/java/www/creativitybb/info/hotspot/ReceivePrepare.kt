package www.creativitybb.info.hotspot

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_receive_prepare.*
import java.lang.reflect.Method


class ReceivePrepare : AppCompatActivity() {
    lateinit var  wifiManager:WifiManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receive_prepare)
        wifiManager = this.getSystemService(Context.WIFI_SERVICE) as WifiManager
         showGpsTick()
        showHotspotTick()
        gps.setOnClickListener {
            openGpsScreen()
        }

        hotspot.setOnClickListener {
            openHotspotScreen()

        }
        goHeadEnabled()
        go_head.setOnClickListener {
            val isGood = isHotSpotEnabled() && isLocationEnabled(this)!!
            if(!isGood){
                goHeadEnabled()
            }else{
                val intent = Intent(this,Receive::class.java)
                startActivity(intent)
            }

        }


    }

    private fun goHeadEnabled(){
        val isGood = isHotSpotEnabled() && isLocationEnabled(this)!!
         if(!isGood){
             showGpsTick()
             showHotspotTick()
         }

    }

    private fun showGpsTick(){

        val isGpsEnabled = isLocationEnabled(this)!!
        if(isGpsEnabled){
            ok_gps.visibility = View.VISIBLE
            gps.visibility = View.GONE
        }else{
            gps.visibility = View.VISIBLE
            ok_gps.visibility = View.GONE
        }

    }
    private fun showHotspotTick(){
        val isHotSpotEnabled = isHotSpotEnabled()
        if(isHotSpotEnabled){
            hotspot.visibility = View.GONE
            ok_hotspot.visibility = View.VISIBLE
        }else{
            hotspot.visibility = View.VISIBLE
            ok_hotspot.visibility = View.GONE
        }
    }
        private fun isHotSpotEnabled():Boolean{

            val method: Method = wifiManager.javaClass.getDeclaredMethod("getWifiApState")
            method.setAccessible(true)
            val actualState = method.invoke(wifiManager) as Int
            return actualState == 13 || actualState == 12
        }


    private fun openHotspotScreen(){
        val intent = Intent(Intent.ACTION_MAIN, null)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val cn = ComponentName("com.android.settings", "com.android.settings.TetherSettings")
        intent.component = cn
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivityForResult(intent,1)
    }

    private fun openGpsScreen(){
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivityForResult(intent,1)
    }

    fun isLocationEnabled(context: Context): Boolean? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // This is new method provided in API 28
            val lm =
                context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            lm.isLocationEnabled
        } else {
            val mode = Settings.Secure.getInt(
                context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                Settings.Secure.LOCATION_MODE_OFF
            )
            mode != Settings.Secure.LOCATION_MODE_OFF
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        showGpsTick()
        showHotspotTick()
        goHeadEnabled()
    }

}