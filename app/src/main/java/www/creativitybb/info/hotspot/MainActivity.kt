package www.creativitybb.info.hotspot

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.StatFs
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(!isTherePermission()){
            getLocationPermission()
        }
        val memory = memoryAvailable()
        free_memory.setText(memory.toString() +" "+ "GB")
        Log.i("bharath",sd_card_free().toString())

        val storage = sd_card_free()
        free_storage.setText(storage.toString() + " "+"GB")
        recieve.setOnClickListener {
            val intent = Intent(this,ReceivePrepare::class.java)
            startActivity(intent)
        }

        send.setOnClickListener {
            val intent = Intent(this,Search::class.java)
            startActivity(intent)
        }
    }
    private fun memoryAvailable():String{
        val mi = ActivityManager.MemoryInfo()
        val  activity:ActivityManager =  getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        activity.getMemoryInfo(mi)
        val availableMegs: Double = (mi.availMem / 1048576L).toDouble()
        val decimalPoint = availableMegs / 1024
        return  String.format("%.2f", decimalPoint);
    }

    fun sd_card_free(): Long {
        val path: File = Environment.getExternalStorageDirectory()
        val stat = StatFs(path.getPath())
        val availBlocks = stat.availableBlocksLong.toInt()
        val blockSize = stat.blockSizeLong.toInt()
        val storage = availBlocks.toLong() * blockSize.toLong()
        val totalMB = storage / 1024
        val toGB = (totalMB / 1024) / 1024
        return toGB
    }

    private fun isTherePermission():Boolean{
        return ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun getLocationPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE),1)
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 1){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,"no permission granted",Toast.LENGTH_SHORT).show()
            }
        }
    }
}

