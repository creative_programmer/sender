package www.creativitybb.info.hotspot

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.wifi.ScanResult
import android.net.wifi.WifiConfiguration
import android.os.Build
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.postDelayed
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.avatar.view.*
import kotlinx.android.synthetic.main.dialog.*
import java.util.*


class AvatarAdapter(val context: Search, val item:List<ScanResult>) : RecyclerView.Adapter<AvatarAdapter.ChildView>() {
    val   WPA2:String = "WPA2"
    val  WPA:String = "WPA"
    val WEP:String = "WEP"
    val OPEN:String = "Open"
    val WPA_EAP = "WPA-EAP"
    val IEEE8021X = "IEEE8021X";

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildView {
        val inflator = LayoutInflater.from(context).inflate(R.layout.avatar, parent, false)
        return ChildView(inflator)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    fun findNetworkType(scanResult: ScanResult): String? {
        val cap = scanResult.capabilities
        val securityModes =
            arrayOf<String>(WEP, WPA, WPA2, WPA_EAP, IEEE8021X)
        for (i in securityModes.indices.reversed()) {
            if (cap.contains(securityModes[i])) {
                return securityModes[i]
            }
        }
        return OPEN
    }


    @RequiresApi(Build.VERSION_CODES.Q)
    @SuppressLint("MissingPermission")
    override fun onBindViewHolder(holder: ChildView, position: Int) {
        val current = item[position]
     holder.item.avatar_name.setText(current.SSID)
        holder.item.setOnClickListener {
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.dialog)
            val networkType = findNetworkType(current)
            if(networkType == OPEN){
                dialog.password.visibility = View.GONE
            }
            dialog.show()
            dialog.connect.setOnClickListener {
                val conf = WifiConfiguration()
                conf.status = WifiConfiguration.Status.ENABLED
                conf.priority = 40
                val ssid = current.SSID
                conf.SSID = "\"" + ssid + "\"";
                if(networkType == OPEN){
                    conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                }
                else if (networkType == WEP){
                    Log.i("bharath","wep")
                    val password = dialog.password.text.toString()
                    conf.wepKeys[0] = "\"" + password + "\"";
                    conf.wepTxKeyIndex = 0;
                    conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                    conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                }else if(networkType == WPA){
                    Log.i("bharath","wpa")
                    val password = dialog.password.text.toString()
                    conf.SSID = "\"" + ssid + "\"";
                    conf.preSharedKey = "\""+ password +"\"";
                }else if(networkType == WPA2){
                    Log.i("bharath","wpa2")
                    val password = dialog.password.text.toString()
                    conf.SSID = "\"" + ssid + "\"";
                    conf.preSharedKey = "\""+ password +"\"";
                }else if(networkType == WPA_EAP){
                    Log.i("bharath","wpa_eap")
                    val password = dialog.password.text.toString()
                    conf.SSID = "\"" + ssid + "\"";
                    conf.preSharedKey = "\""+ password +"\"";
                }else{
                        Toast.makeText(context,"unsupported AP type",Toast.LENGTH_SHORT).show()
                       dialog.dismiss()
                       dialog.cancel()
                }

                context.wifiManager.addNetwork(conf)
                val list = context.wifiManager.configuredNetworks
                for (i in list) {
                if (i.SSID != null && i.SSID == "\"" + ssid + "\"") {
                    context.wifiManager.disconnect()
                    context.wifiManager.enableNetwork(i.networkId, true)
                    if(context.wifiManager.reconnect()){
                        val intent = Intent(context,Upload::class.java)
                        context.startActivity(intent)
                    }

                }
            }
            }
        }
    }

    class ChildView(view: View) : RecyclerView.ViewHolder(view) {
        val item = view.avatar_container
    }

}