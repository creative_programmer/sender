package www.creativitybb.info.hotspot

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_receive.*
import java.io.File
import java.io.FileOutputStream
import java.io.ObjectInputStream
import java.net.ServerSocket
import java.net.Socket

class Receive : AppCompatActivity() {

    var name:ArrayList<String>? = null
    var size:ArrayList<Long>? = null
    var fileobject:ArrayList<FileObject>? = null
    var adapter:SendingAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receive)
        val scaleAnimation = ScaleAnimation(1f,0.9f,1f,0.9f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)

            scaleAnimation.repeatMode = Animation.REVERSE
            scaleAnimation.repeatCount = Animation.INFINITE
            scaleAnimation.duration = 1000
            behind_middle_circle.startAnimation(scaleAnimation)
            front_circle.startAnimation(scaleAnimation)
            behind_circle_r.startAnimation(scaleAnimation)

        ServerThread(this).start()
    }


    fun constructItem(){
        fileobject.let {
            adapter = SendingAdapter(this,fileobject!!)
            recieve_recycle.adapter = adapter
            recieve_recycle.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,false)
        }
    }

    fun constructFileObject(){
        if(name!= null && size!= null){
            var ifileobject = ArrayList<FileObject>()
            name!!.zip(size!!){ s: String, l: Long ->
                ifileobject.add(
                    FileObject(
                        name = s,
                        size = l
                    )
                )
            }
            fileobject = ifileobject
        }
    }
}



class ServerThread(val context: Receive) : Thread(){
    lateinit var socket: Socket
    lateinit var serverSocket: ServerSocket
    @RequiresApi(Build.VERSION_CODES.Q)
    override fun run() {
        super.run()
        try {
            serverSocket = ServerSocket(8888)
            socket = serverSocket.accept()
            val input = socket.getInputStream()
            val inputStream  = ObjectInputStream(input)
            val filename = inputStream.readObject() as ArrayList<String> //filename
            context.name = filename
            val filesize = inputStream.readObject() as ArrayList<Long> //filesize
            context.size = filesize
            context.runOnUiThread{
                val circle = context.findViewById<FrameLayout>(R.id.behind_circle_r)
                circle.clearAnimation()
                circle.visibility = View.GONE

                context.findViewById<RecyclerView>(R.id.recieve_recycle).visibility = View.VISIBLE
            }
            context.constructFileObject()
            context.runOnUiThread {
                context.constructItem()
            }

            filename.forEach {
                val file = Environment.getExternalStorageDirectory().toString()+"/"+context.applicationContext.packageName + "/" + it
                Log.i("info",file)
                val fileCreate = File(file)
                if(!fileCreate.parentFile.exists()){
                    fileCreate.parentFile.mkdir()
                }
                if(!fileCreate.exists()){
                    fileCreate.createNewFile()
                }
                val outputStream = FileOutputStream(fileCreate)
                val buffer = ByteArray(2468)
                while (inputStream!!.read(buffer) != null){
                    outputStream.write(buffer)
                    outputStream.flush()
                }
//                inputStream.copyTo(outputStream)
                outputStream.close()
            }
            context.runOnUiThread {
                context.findViewById<TextView>(R.id.receive_text).setText("Received")
                Toast.makeText(context,"file has been recieved", Toast.LENGTH_SHORT).show()
            }
            inputStream.close()
            socket.close()
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}