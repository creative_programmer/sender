package www.creativitybb.info.hotspot

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_sending.*
import java.io.File
import java.io.ObjectOutputStream
import java.net.InetSocketAddress
import java.net.Socket
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong

//object Progress{
//    var Progress = 0
//    var FILE = -1
//}

data class Progress(var Progress:AtomicLong = AtomicLong(0), var FILE:AtomicInteger= AtomicInteger(-1))

class Sending : AppCompatActivity() {

    var items:ArrayList<FileObject>? = null
    var progress:Progress = Progress()
    lateinit var adapter:SendingAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sending)
        items = intent.getSerializableExtra("data") as ArrayList<FileObject>
        if(items!=null && items!=null){
            adapter = SendingAdapter(this,items!!)
            sending.adapter = adapter
            sending.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
            val size = getFileSize(items!!)
            val name = getFileName(items!!)
            val path = getFilePath(items!!)
            ClientThread(this,size,path,name).start()
        }
    }
    private fun getFileSize(file:ArrayList<FileObject>): ArrayList<Long> {
        val fileSize:ArrayList<Long> = ArrayList()
        file.forEach {
            fileSize.add(it.size)
        }
        return fileSize
    }

    private fun getFileName(file:ArrayList<FileObject>):ArrayList<String>{
        val filename:ArrayList<String> = ArrayList()
        file.forEach {
            val thefile = File(it.path)
            filename.add(thefile.name)
        }
        return filename
    }

    private fun getFilePath(file:ArrayList<FileObject>):ArrayList<String>{
        val filename:ArrayList<String> = ArrayList()
        file.forEach {
            filename.add(it.path)
        }
        return filename
    }
   fun sentProgress(total:Long){
             Log.i("bharath","Total File Size"+total)
             Log.i("bharath","progress.Progress"+progress.Progress)
             val percentage = ((progress.Progress.get() * 100) /  total)
             Log.i("bharath","percentage"+percentage.toString())
            adapter.item[progress.FILE.get()].progress = percentage.toInt()
            adapter.notifyDataSetChanged()

    }
}

class ClientThread(
    val context:Sending ,
    val filesize: ArrayList<Long>, val filepath:ArrayList<String>,val filename:ArrayList<String>): Thread(){
    lateinit var socket: Socket
    @RequiresApi(Build.VERSION_CODES.Q)
    override fun run() {
        super.run()
        try {
            socket = Socket()
            socket.bind(null)
            socket.connect(InetSocketAddress("192.168.43.1",8888),5000)
            val output = socket.getOutputStream()
            val contextResolver = context.applicationContext.contentResolver
            val objectoutPutStream = ObjectOutputStream(output)
            objectoutPutStream.writeObject(filename)
            objectoutPutStream.flush()
            objectoutPutStream.writeObject(filesize)
            objectoutPutStream.flush()
            filepath.zip(filesize) { it: String, s1: Long ->
                context.progress.FILE.incrementAndGet()
                val my = File(it)
                if(my.exists()){
                    val inputstreamFile = contextResolver.openInputStream(Uri.fromFile(my))
                    val buffer = ByteArray(2468)
                    buffer
                    var len:Long = 0
                    while (inputstreamFile!!.read(buffer).also{
                                value->
                                len = value.toLong()
                        } != -1){
                        objectoutPutStream.write(buffer)
                        objectoutPutStream.flush()

                        context.runOnUiThread {
                            context.progress.Progress.addAndGet(len)
                            context.sentProgress(s1)
                        }
                    }
                    inputstreamFile!!.close()
                }
                context.progress.Progress.set(0)
            }
            context.runOnUiThread {
                context.findViewById<TextView>(R.id.send_text).setText("Sent")
                Toast.makeText(context,"file has been sent", Toast.LENGTH_SHORT).show()
            }

            objectoutPutStream.close()
            socket.close()

        }catch (e: Exception){
            e.printStackTrace()

        }
    }
}
