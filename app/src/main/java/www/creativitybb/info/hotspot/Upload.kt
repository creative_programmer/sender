package www.creativitybb.info.hotspot


import android.os.Bundle
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.*
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_upload.*
import java.io.File
import java.text.DecimalFormat

object FILEConst{
    val UPLOAD = 1
    fun getPath(context: Context, uri: Uri): String? {
        val isKitKat: Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
            } else if (isDownloadsDocument(uri)) {
                val id: String = DocumentsContract.getDocumentId(uri)
                val contentUri: Uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1]
                )
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } else if ("content".equals(uri.getScheme(), ignoreCase = true)) {
            return getDataColumn(context, uri, null, null)
        } else if ("file".equals(uri.getScheme(), ignoreCase = true)) {
            return uri.getPath()
        }
        return null
    }


    fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            cursor = context.contentResolver.query(
                uri!!, projection, selection, selectionArgs,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val column_index: Int = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(column_index)
            }
        } finally {
            if (cursor != null) cursor.close()
        }
        return null
    }



    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.getAuthority()
    }


    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.getAuthority()
    }


    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.getAuthority()
    }

}

class Upload : AppCompatActivity() {
    val Filename = ArrayList<String>()
    val FilePath = ArrayList<String>()
    val FileSize =  ArrayList<Long>()
    val fileobject = ArrayList<FileObject>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)
        upload_icon.setOnClickListener {
            val i = Intent(Intent.ACTION_GET_CONTENT)
            i.type = "*/*"
            startActivityForResult(i, FILEConst.UPLOAD)
        }
        choose_file.setOnClickListener {
            val i = Intent(Intent.ACTION_GET_CONTENT)
            i.type = "*/*"
            startActivityForResult(i, FILEConst.UPLOAD)
        }

        send_button.setOnClickListener {
            if(fileobject.size < 1){
                Toast.makeText(this,"no file choosen",Toast.LENGTH_SHORT).show()
            }else{
                val intent = Intent(this,Sending::class.java)
                intent.putExtra("data",fileobject)
                startActivity(intent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == FILEConst.UPLOAD){
            if(resultCode == Activity.RESULT_OK){
                val file = data!!.data!!
                val absolutePath = FILEConst.getPath(this,file)
                if(absolutePath!= null){
                    val file = File(absolutePath)
                    if(file.exists()){
                        Filename.add(file.name)
                        FilePath.add(absolutePath!!)
                        FileSize.add(file.length())
                        Log.i("bharath",file.length().toString())
                        fileobject.add(FileObject(file.name,file.length(),absolutePath))
                        constructIcon()
                        val value = countTotalSize().toString()
                        send_button.setText("SEND($value MB)")
                    }
                }
            }
        }

    }

    fun constructIcon(){
        val adapter = IconAdapter(this,fileobject)
        file_info.layoutManager = GridLayoutManager(this,3,GridLayoutManager.VERTICAL,false)
        file_info.adapter = adapter
    }

    fun countTotalSize(): Double{
        val total = FileSize.reduce{ l: Long, l1: Long ->
            l+l1
        }
        val kb = total / 1024
        val howmanyMB = kb.toDouble() / 1024
        val decimalFormat = DecimalFormat("#.#")

        return  decimalFormat.format(howmanyMB).toDouble()
    }
}
