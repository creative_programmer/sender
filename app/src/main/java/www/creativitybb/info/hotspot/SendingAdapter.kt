package www.creativitybb.info.hotspot

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.file_icon.view.*
import kotlinx.android.synthetic.main.file_info.view.*
import kotlinx.android.synthetic.main.file_info.view.file_name
import java.io.Serializable
import java.text.DecimalFormat


class SendingAdapter(val context: Context,val item:ArrayList<FileObject>) : RecyclerView.Adapter<SendingAdapter.ChildView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildView {
        val inflator = LayoutInflater.from(context).inflate(R.layout.file_info,parent,false)
        return SendingAdapter.ChildView(inflator)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(holder: ChildView, position: Int) {
        val current = item[position]
        if(current.name.length > 20){
            val substring = current.name.substring(0,18) + "..."
            holder.item.file_name.setText(substring)
        }else{
            holder.item.file_name.setText(current.name)
        }
        holder.item.progress_bar.progress = current.progress
        val kb = current.size / 1024
        val howmanyMB = kb.toDouble() / 1024
        val decimalFormat = DecimalFormat("#.#")
        val size = decimalFormat.format(howmanyMB).toDouble()
        holder.item.file_size.setText(size.toString()+"MB")
        val dot = current.name.lastIndexOf(".")
        val fileExtension = current.name.substring(dot+1)
        when(fileExtension){
            "apk"->holder.item.file_image.setImageResource(R.drawable.ic_android)
            "bmp" -> holder.item.file_image.setImageResource(R.drawable.ic_camera_1908)
            "gif" -> holder.item.file_image.setImageResource(R.drawable.ic_camera_1908)
            "pdf" -> holder.item.file_image.setImageResource(R.drawable.ic_pdf)
            "mp4" -> holder.item.file_image.setImageResource(R.drawable.ic_mp4_1522)
            "jpg" ->  holder.item.file_image.setImageResource(R.drawable.ic_camera_1908)
            "jpeg" ->  holder.item.file_image.setImageResource(R.drawable.ic_camera_1908)
            else -> holder.item.file_image.setImageResource(R.drawable.ic_file_1500)
        }



    }

    class ChildView(val view: View) : RecyclerView.ViewHolder(view){
        val item = view.file_container
    }


}



data class FileObject(var name:String,var size:Long,var path:String="",var progress:Int = 0) : Serializable