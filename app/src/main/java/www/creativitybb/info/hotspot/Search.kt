package www.creativitybb.info.hotspot

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.NetworkInfo
import android.net.wifi.ScanResult
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_search.*



class Search : AppCompatActivity() {
    lateinit var wifiManager: WifiManager
    lateinit var members:List<ScanResult>
    val mWifiScanReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        @SuppressLint("MissingPermission")
        override fun onReceive(c: Context, intent: Intent) {
            val action = intent.getAction()
            Log.i("bharath",action)
            if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                val scanResults: List<ScanResult> = wifiManager.scanResults
                if(scanResults.size > 0){
                    members = scanResults
                    constructMember()
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        wifiManager = this.getSystemService(Context.WIFI_SERVICE) as WifiManager
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q){
            if(!wifiManager.isWifiEnabled){
                val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                startActivity(intent)
            }
        }else{
            if(!wifiManager.isWifiEnabled){
                wifiManager.setWifiEnabled(true)
            }
        }

        start.setOnClickListener {
           if(wifiManager.isWifiEnabled){
               val scaleAnimation = ScaleAnimation(1f,0.9f,1f,0.9f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
               scaleAnimation.repeatMode = Animation.REVERSE
               scaleAnimation.repeatCount = Animation.INFINITE
               scaleAnimation.duration = 1000
               behind_middle_circle.startAnimation(scaleAnimation)
               front_circle.startAnimation(scaleAnimation)
               behind_circle.startAnimation(scaleAnimation)
               val intentFilter = IntentFilter()
               intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
               intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);

               registerReceiver(mWifiScanReceiver,
                   intentFilter
               );

               wifiManager.startScan()
           }else{
               Toast.makeText(this,"please enable wifi",Toast.LENGTH_SHORT).show()
           }
        }
    }


    private fun constructMember(){
        val adapter = AvatarAdapter(this,members)
        member_recycle.adapter = adapter
        member_recycle.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
    }



}